using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Amazon.Lambda.Core;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.SQS;
using Amazon.XRay.Recorder.Handlers.AwsSdk;
using OpenTelemetry;
using OpenTelemetry.Contrib.Extensions.AWSXRay.Trace;
using OpenTelemetry.Contrib.Instrumentation.AWSLambda.Implementation;
using OpenTelemetry.Trace;
using Serilog;
using Serilog.Core;
using Serilog.Enrichers.Span;
using Serilog.Sink.LambdaLogger;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace LambdaTestWithApigateway
{
    public class Functions
    {
        private const string E_SQS_QUEUE_URL = "SQS_QUEUE_URL";
        private const string E_USE_OTEL = "USE_OTEL";
        private readonly TracerProvider _tracerProvider;
        private readonly Logger _logger;
        private readonly bool _useOtel;

        private static readonly ActivityListener Listener = new()
        {
            ShouldListenTo = _ => true,
            Sample = (ref ActivityCreationOptions<ActivityContext> _) => ActivitySamplingResult.AllData,
        };
        /// <summary>
        /// Default constructor that Lambda will invoke.
        /// </summary>
        public Functions()
        {
            AppContext.SetSwitch(
                "System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);

            _logger = new LoggerConfiguration()
                .Enrich.WithSpan(new SpanOptions
                {
                    LogEventPropertiesNames = new SpanLogEventPropertiesNames
                    {
                        TraceId = "@corr_trace_id",
                        SpanId = "@corr_span_id",
                        ParentId = "@corr_parent_id"
                    }
                })
                .Enrich.FromLogContext()
                .WriteTo.Console(new AstJsonFormatter())
                .CreateLogger();
            ActivitySource.AddActivityListener(Listener);
            _useOtel = Environment.GetEnvironmentVariable(E_USE_OTEL) == "TRUE" ;
            if (_useOtel)
            {
                _tracerProvider = Sdk.CreateTracerProviderBuilder()
                    .AddAWSLambdaConfigurations()
                    .AddOtlpExporter(otlpOptions =>
                    {
                        otlpOptions.Endpoint = new Uri("http://localhost:4317");
                    })
                    .Build();
                Sdk.SetDefaultTextMapPropagator(new AWSXRayPropagator());
            }
            AWSSDKHandler.RegisterXRayForAllServices();
        }
        
        // Lambda function handler wrapper passed in
        public Task<APIGatewayProxyResponse> TracingFunctionHandler(APIGatewayProxyRequest request,
            ILambdaContext context)
        {
            return AWSLambdaWrapper.Trace(_tracerProvider, Get, request, context);
        }


        /// <summary>
        /// A Lambda function to respond to HTTP Get methods from API Gateway
        /// </summary>
        /// <param name="request"></param>
        /// <returns>The API Gateway response.</returns>
        private async Task<APIGatewayProxyResponse> Get(APIGatewayProxyRequest request, ILambdaContext context)
        {
            _logger.Information("started at {Time}", DateTime.UtcNow);
           
            try
            {
                if (request.QueryStringParameters != null)
                {
                    var parameter = request.QueryStringParameters
                        .FirstOrDefault(o => o.Key.Equals("error", StringComparison.CurrentCultureIgnoreCase));
                    var errorRequested =
                        parameter.Value?.Equals("true", StringComparison.CurrentCultureIgnoreCase) ?? false;
                    if (errorRequested)
                        throw new Exception("Test Exception Error");
                }

                var sqsClient = new AmazonSQSClient();
                var queue = Environment.GetEnvironmentVariable(E_SQS_QUEUE_URL);
                _logger.Debug("queue is {Queue}", queue);

                var responseSendMsg = await sqsClient.SendMessageAsync(queue, "GO");

                var response = new APIGatewayProxyResponse
                {
                    StatusCode = (int) HttpStatusCode.OK,
                    Body =
                        $"Echo: x-amzn-trace-id = {GetHeaderValue(request, "x-amzn-trace-id")} and traceparent = {GetHeaderValue(request, "traceparent")} \r\n to go request was sent with status {responseSendMsg.HttpStatusCode.ToString()}",
                    Headers = new Dictionary<string, string> {{"Content-Type", "text/plain"}}
                };
                _logger.Debug("queue is {Queue}", queue);
                return response;
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error while processing Get");
                throw;
            }
           
        }

        private static string GetHeaderValue(APIGatewayProxyRequest request, string header)
        {
            var result = request.Headers
                .FirstOrDefault(o => o.Key.Equals(header, StringComparison.OrdinalIgnoreCase)).Value;
            return result;
        }
    }
}