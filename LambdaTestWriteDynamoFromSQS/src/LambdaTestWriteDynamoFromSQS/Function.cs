using System;
using System.Linq;
using System.Threading.Tasks;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.Lambda.Core;
using Amazon.Lambda.SQSEvents;
using OpenTelemetry;
using OpenTelemetry.Contrib.Extensions.AWSXRay.Trace;
using OpenTelemetry.Contrib.Instrumentation.AWSLambda.Implementation;
using OpenTelemetry.Trace;


// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace LambdaTestWriteDynamoFromSQS
{
    public class Function
    {
        private readonly TracerProvider _tracerProvider;
        public const string E_TABLENAME = "ATable";
        IDynamoDBContext DdbContext { get; set; }
        /// <summary>
        /// Default constructor. This constructor is used by Lambda to construct the instance. When invoked in a Lambda environment
        /// the AWS credentials will come from the IAM role associated with the function and the AWS region will be set to the
        /// region the Lambda function is executed in.
        /// </summary>
        public Function()
        {
            _tracerProvider = Sdk.CreateTracerProviderBuilder()
                // add other instrumentations
                
                .AddXRayTraceId()
                .AddAWSLambdaConfigurations()
                .AddOtlpExporter(otlpOptions =>
                {
                    otlpOptions.Endpoint = new Uri("http://localhost:4317");
                })
                // .SetResourceBuilder(ResourceBuilder
                //     .CreateDefault()
                //     .AddDetector(new AWSLambdaResourceDetector()))
                .Build();
            Sdk.SetDefaultTextMapPropagator(new AWSXRayPropagator());
            //AWSSDKHandler.RegisterXRayForAllServices();
            // Check to see if a table name was passed in through environment variables and if so 
            // add the table mapping.
            var tableName = System.Environment.GetEnvironmentVariable(E_TABLENAME);
            if (!string.IsNullOrEmpty(tableName))
            {
                AWSConfigsDynamoDB.Context.TypeMappings[typeof(GoRecord)] =
                    new Amazon.Util.TypeMapping(typeof(GoRecord), tableName);
            }

            var config = new DynamoDBContextConfig {Conversion = DynamoDBEntryConversion.V2};
            this.DdbContext = new DynamoDBContext(new AmazonDynamoDBClient(), config);
        }

        public Task TracingFunctionHandler(SQSEvent evnt, ILambdaContext context)
        {
            context.Logger.LogLine($"_X_AMZN_TRACE_ID {Environment.GetEnvironmentVariable("_X_AMZN_TRACE_ID")}");
            var first = evnt.Records.FirstOrDefault();
            if (first != null)
            {
                Environment.SetEnvironmentVariable("_X_AMZN_TRACE_ID",first.Attributes["AWSTraceHeader"]);
                context.Logger.LogLine($"_X_AMZN_TRACE_ID {Environment.GetEnvironmentVariable("_X_AMZN_TRACE_ID")}");
            }
            return AWSLambdaWrapper.Trace(_tracerProvider, FunctionHandler, evnt, context);
        }

        /// <summary>
        /// This method is called for every Lambda invocation. This method takes in an SQS event object and can be used 
        /// to respond to SQS messages.
        /// </summary>
        /// <param name="evnt"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task FunctionHandler(SQSEvent evnt, ILambdaContext context)
        {
            foreach (var message in evnt.Records)
            {
                await ProcessMessageAsync(message, context);
            }
        }

        private async Task ProcessMessageAsync(SQSEvent.SQSMessage message, ILambdaContext context)
        {
            context.Logger.LogLine($"Processed message {message.Body}");
            
            var goRecord = new GoRecord
            {
                Id = message.MessageId,
                CreatedTimestamp = DateTime.Now
            };

            context.Logger.LogLine($"Saving record with id {goRecord.Id}");
            await DdbContext.SaveAsync(goRecord);
          
            await Task.CompletedTask;
        }
    }

    public class GoRecord
    {
        public string Id { get; set; }
        public DateTime CreatedTimestamp { get; set; }
    }
    

}