using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using OpenTelemetry;
using OpenTelemetry.Contrib.Extensions.AWSXRay.Trace;
using OpenTelemetry.Resources;
using OpenTelemetry.Trace;

namespace ASPNetCoreTraceableAPI.Controllers;

[ApiController]
[Route("[controller]")]
public class GoController : ControllerBase
{
    private readonly HttpClient _httpClient = new();

    private readonly ILogger<GoController> _logger;

    public GoController(ILogger<GoController> logger)
    {
        _logger = logger;
    }

    [HttpGet("[action]")]
    public async Task<ActionResult<string>> Ping()
    {
        try
        {
            var traceHeader = HttpContext.Request.Headers
                .FirstOrDefault(o => o.Key.Equals("x-amzn-trace-id", StringComparison.OrdinalIgnoreCase)).Value;
            //a span is already created with parentSpan and trace id in according to x-amzn-trace-id header, thanks to AspNetCoreInstrumentation
            _logger.LogDebug("@corr-parent-id = {ParentId}, @corr-span-id = {SpanId}, @corr-trace-id = {TraceId}",
                Activity.Current!.ParentId,
                Activity.Current.SpanId.ToString(), Activity.Current.TraceId.ToString());
            var result = traceHeader.ToString();
            return string.IsNullOrWhiteSpace(result) ? "simple echo" : result;
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error while processing Ping request");
            return BadRequest("Error in ping");
        }
    }
    
    [HttpGet("[action]")]
    public async Task<ActionResult<string>> LambdaNoVpc()
    {
        //this is an example span to demonstrate
        var activity =
            Activity.Current!.Source.StartActivity("Test span in Get lambda");
        try
        {
            var currentActivity = Activity.Current;
            _logger.LogInformation(
                "@corr-parent-id = {@corr-parent-id}, @corr-span-id = {@corr-span-id}, @corr-trace-id = {@corr-trace-id}, isRecorded={@corr-trace-is-recorded}",
                currentActivity.ParentId,
                currentActivity.SpanId.ToString(),
                currentActivity.TraceId.ToString(),
                currentActivity.Recorded);
            //thanks to HttpClientInstrumentation the request will be sent with x-amzn-trace-id which carry info about current span
            var lambdaUrl = Environment.GetEnvironmentVariable("LAMBDA_URL");
            var response =
                await _httpClient.GetAsync($"{lambdaUrl}api/v1/testNoVPC");
            return await response.Content.ReadAsStringAsync();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error while processing get request");
            return BadRequest("Error in get");
        }
        finally
        {
            if (activity != null)
            {
                activity.Stop();
                _logger.LogInformation("{Activity} ended in {Duration}", activity.DisplayName,
                    activity.Duration.ToString("g"));
            }
        }
    }

    [HttpGet("[action]")]
    public async Task<ActionResult<string>> LambdaVpc()
    {
        try
        {
            var lambdaUrl = Environment.GetEnvironmentVariable("LAMBDA_URL");
            var response =
                await _httpClient.GetAsync($"{lambdaUrl}api/v1/testVPC");
            return await response.Content.ReadAsStringAsync();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error while processing get request");
            return BadRequest("Error in get");
        }
    }
    
    [HttpGet("[action]")]
    public async Task<ActionResult<string>> LambdaXray()
    {
        try
        {
            var lambdaUrl = Environment.GetEnvironmentVariable("LAMBDA_URL");
            var response =
                await _httpClient.GetAsync($"{lambdaUrl}api/v1/testXray");
            return await response.Content.ReadAsStringAsync();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error while processing get request");
            return BadRequest("Error in get");
        }
    }
    
    [HttpGet("[action]")]
    public async Task<ActionResult<string>> LambdaXrayVpc()
    {
        try
        {
            var lambdaUrl = Environment.GetEnvironmentVariable("LAMBDA_URL");
            var response =
                await _httpClient.GetAsync($"{lambdaUrl}api/v1/testXrayVPC");
            return await response.Content.ReadAsStringAsync();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error while processing get request");
            return BadRequest("Error in get");
        }
    }
}