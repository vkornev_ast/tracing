using ASPNetCoreTraceableAPI.Controllers;
using OpenTelemetry;
using OpenTelemetry.Contrib.Extensions.AWSXRay.Trace;
using OpenTelemetry.Resources;
using OpenTelemetry.Trace;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
Sdk.CreateTracerProviderBuilder()
    //setup attributes to be use along with tracing ids
    // ASP.NETCoreTracableApp - is the name for service 
    .SetResourceBuilder(ResourceBuilder
        .CreateDefault()
        .AddService("ASP.NETCoreTracableApp")
        .AddTelemetrySdk())
    //X-Ray compatible correlation ids
    .AddXRayTraceId()
    // add instrumentations for  controllers
    .AddAspNetCoreInstrumentation()
    // add instrumentations for http requests. If you need to instrument other clients - just try to search OpenTelemetry.Instrumentation packages.
    .AddHttpClientInstrumentation()
    // setup of side-car which will get data in OTLP format
    .AddOtlpExporter(options =>
    {
        options.Endpoint = new Uri("http://localhost:4317");
    })
    .Build();
//propagator to send x-amzn-trace-id header instead of W3C header which is in use for OpenTelemetry
Sdk.SetDefaultTextMapPropagator(new AWSXRayPropagator());


var corsUrl = Environment.GetEnvironmentVariable("CORS_URL");

// builder.Services.AddCors(options =>
// {
//     options.AddDefaultPolicy(
//         o =>
//         {
//             o.WithOrigins($"https://{corsUrl}");
//         });
// });

var app = builder.Build();

var logger  = app.Services.GetRequiredService<ILogger<GoController>>();
logger.LogInformation("test");
logger.LogInformation("corsUrl: {CorsUrl}",corsUrl);

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

//app.UseCors();

app.UseAuthorization();

app.MapControllers();

app.Run();