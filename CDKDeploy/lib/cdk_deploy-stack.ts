import {CfnOutput, Duration, Stack, StackProps} from 'aws-cdk-lib';
import * as sqs from 'aws-cdk-lib/aws-sqs';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as path from "path";
import * as awsLogs from "aws-cdk-lib/aws-logs";
import * as api from "aws-cdk-lib/aws-apigateway";
import * as dynamo from "aws-cdk-lib/aws-dynamodb";
import {Construct} from 'constructs';
import {AstNaming} from "../AstNaming";
import {dotnet, npm} from "@ast/shellts";
import * as shell from "@ast/shellts/lib/shellJsWapper"
import {SqsEventSource} from "aws-cdk-lib/aws-lambda-event-sources";
import * as s3Deploy from "aws-cdk-lib/aws-s3-deployment";
import * as s3 from "aws-cdk-lib/aws-s3";
import * as cloudfront from "aws-cdk-lib/aws-cloudfront";
import {OriginProtocolPolicy} from "aws-cdk-lib/aws-cloudfront";
import {HttpOrigin, S3Origin} from "aws-cdk-lib/aws-cloudfront-origins";
import * as cdk8s from 'cdk8s';
import * as eks from "aws-cdk-lib/aws-eks";
import {k8sChart} from "./k8sChart";
import * as ecrAssets from "aws-cdk-lib/aws-ecr-assets";
import {Vpc} from "aws-cdk-lib/aws-ec2";
import * as cdk from '@aws-cdk/core';


export class CdkDeployStack extends Stack {
    constructor(scope: Construct, id: string, props?: StackProps) {
        super(scope, id, props);
        AstNaming.applicationName = "obs";
        AstNaming.componentName = "webapp";
        AstNaming.envName = "m-tech-dev-archkorn";

        const sqsQueue = new sqs.Queue(this, AstNaming.Name('TracedQueue'), {
            visibilityTimeout: Duration.seconds(300),
            retentionPeriod: Duration.hours(2)
        });
        new CfnOutput(this, 'sqsUrl', {
            value: sqsQueue.queueArn
        });
        const adotLayer = lambda.LayerVersion.fromLayerVersionArn(this, "ADOT",
            `arn:aws:lambda:${this.region}:901920570463:layer:aws-otel-collector-ver-0-39-0:1`
        );
        const vpc = Vpc.fromLookup(this, "eks-vkornev-01-vpc", {vpcId: "vpc-0fa48d7eb3984779e"});
        const entryLambdaAsses = CdkDeployStack.lambdaPackage('LambdaTestWithApigateway');
        console.log(entryLambdaAsses);
        const entryLambda = new lambda.Function(this, AstNaming.Name("entryLambda"),
            {
                runtime: lambda.Runtime.DOTNET_CORE_3_1,
                functionName:AstNaming.Name("entryLambda"),
                description: "Run with get request and push data to sqs to test tracing",
                code: lambda.Code.fromAsset(entryLambdaAsses),
                handler: "LambdaTestWithApigateway::LambdaTestWithApigateway.Functions::TracingFunctionHandler",
                tracing: lambda.Tracing.ACTIVE,
                timeout: Duration.seconds(20),
                logRetention: awsLogs.RetentionDays.ONE_DAY,
                environment: {
                    "SQS_QUEUE_URL": sqsQueue.queueUrl,
                    "USE_OTEL":"TRUE",
                    "OPENTELEMETRY_COLLECTOR_CONFIG_FILE":"/var/task/collector_apm.yaml"
                },
                layers: [adotLayer]
            });
        sqsQueue.grantSendMessages(entryLambda);

        const entryLambdaXray = new lambda.Function(this, AstNaming.Name("entryLambdaXray"),
            {
                runtime: lambda.Runtime.DOTNET_CORE_3_1,
                functionName:AstNaming.Name("entryLambdaXray"),
                description: "Run with get request and push data to sqs to test tracing with Xray",
                code: lambda.Code.fromAsset(entryLambdaAsses),
                handler: "LambdaTestWithApigateway::LambdaTestWithApigateway.Functions::Get",
                tracing: lambda.Tracing.ACTIVE,
                timeout: Duration.seconds(20),
                logRetention: awsLogs.RetentionDays.ONE_DAY,
                environment: {
                    "SQS_QUEUE_URL": sqsQueue.queueUrl
                }
            });
        sqsQueue.grantSendMessages(entryLambdaXray);

        const entryLambdaXrayVpc = new lambda.Function(this, AstNaming.Name("entryLambdaXrayVpc"),
            {
                runtime: lambda.Runtime.DOTNET_CORE_3_1,
                functionName:AstNaming.Name("entryLambdaXrayVpc"),
                description: "Run with get request and push data to sqs to test tracing with Xray",
                code: lambda.Code.fromAsset(entryLambdaAsses),
                handler: "LambdaTestWithApigateway::LambdaTestWithApigateway.Functions::Get",
                tracing: lambda.Tracing.ACTIVE,
                timeout: Duration.seconds(20),
                logRetention: awsLogs.RetentionDays.ONE_DAY,
                environment: {
                    "SQS_QUEUE_URL": sqsQueue.queueUrl
                },
                vpc:vpc
            });
        sqsQueue.grantSendMessages(entryLambdaXrayVpc);
        
        const entryLambdaVPC = new lambda.Function(this, AstNaming.Name("entryLambdaVPC"),
            {
                runtime: lambda.Runtime.DOTNET_CORE_3_1,
                functionName:AstNaming.Name("entryLambdaVPC"),
                description: "Run in VPC with get request and push data to sqs to test tracing",
                code: lambda.Code.fromAsset(entryLambdaAsses),
                handler: "LambdaTestWithApigateway::LambdaTestWithApigateway.Functions::TracingFunctionHandler",
                tracing: lambda.Tracing.ACTIVE,
                timeout: Duration.seconds(20),
                logRetention: awsLogs.RetentionDays.ONE_DAY,
                environment: {
                    "SQS_QUEUE_URL": sqsQueue.queueUrl,
                    "USE_OTEL":"TRUE",
                    "OPENTELEMETRY_COLLECTOR_CONFIG_FILE":"/var/task/collector_apm.yaml"
                },
                layers: [adotLayer],
                vpc:vpc
            });
        sqsQueue.grantSendMessages(entryLambdaVPC);
        
        const entryApi = new api.RestApi(this, AstNaming.Name("the-gateway"),
            {
                description: "Tracing test API",
                endpointConfiguration: {types: [api.EndpointType.REGIONAL]},
                defaultMethodOptions: {authorizationType: api.AuthorizationType.NONE},
                restApiName: AstNaming.Name("entryApi"),
                deployOptions: {tracingEnabled: true}
            });
        
        const root = entryApi.root.addResource("api").addResource("v1");
        const resource = root.addResource("testNoVPC");
        resource.addMethod("GET",new api.LambdaIntegration(entryLambda));
        const resource2 = root.addResource("testVPC");
        resource2.addMethod("GET",new api.LambdaIntegration(entryLambdaVPC));
        const resource3 = root.addResource("testXray");
        resource3.addMethod("GET",new api.LambdaIntegration(entryLambdaXray));
        const resource4 = root.addResource("testXrayVPC");
        resource4.addMethod("GET",new api.LambdaIntegration(entryLambdaXrayVpc));
        
        const dynamoTable = new dynamo.Table(this, AstNaming.Name("the-table"), {
            partitionKey: {name: "Id", type: dynamo.AttributeType.STRING},
        });

        const writerLambdaAsses = CdkDeployStack.lambdaPackage('LambdaTestWriteDynamoFromSQS');
        const writerLambda = new lambda.Function(
            this,
            AstNaming.Name("go-with-sqs-to-dynamo"),
            {
                functionName:AstNaming.Name("sqs-to-dynamo"),
                runtime: lambda.Runtime.DOTNET_CORE_3_1,
                description: "Run with sqs event and write data to dynamo to test tracing",
                code: lambda.Code.fromAsset(writerLambdaAsses),
                handler: "LambdaTestWriteDynamoFromSQS::LambdaTestWriteDynamoFromSQS.Function::TracingFunctionHandler",
                tracing: lambda.Tracing.ACTIVE,
                timeout: Duration.seconds(10),
                logRetention: awsLogs.RetentionDays.ONE_DAY,
                environment: {"ATable": dynamoTable.tableName},
                layers: [adotLayer],
                events: [new SqsEventSource(sqsQueue)],
            });
        sqsQueue.grantConsumeMessages(writerLambda);
        dynamoTable.grantFullAccess(writerLambda);
        
        const reactAppAsset = CdkDeployStack.nmpBuild('ReactTracebleApp');
        const appBucket = new s3.Bucket(this, AstNaming.Name("react-app-bucket"), {
            publicReadAccess: true,
            websiteIndexDocument: "index.html",
            removalPolicy: cdk.RemovalPolicy.DESTROY
        });
        
        const distribution = new cloudfront.CloudFrontWebDistribution(this, AstNaming.Name("Distribution"),
            {
                defaultRootObject: "index.html",
               
                // defaultBehavior: {
                //     origin:  new S3Origin(appBucket, {originAccessIdentity: originAccessIdentity}),
                // },
                originConfigs: [{
                    customOriginSource: {
                        domainName: appBucket.bucketWebsiteDomainName,
                        originProtocolPolicy: cloudfront.OriginProtocolPolicy.HTTP_ONLY
                    },
                    behaviors: [{
                        isDefaultBehavior: true
                    }]
                }]
            });
        new s3Deploy.BucketDeployment(this, AstNaming.Name("react-app-bucket-Deployment"),
            {
                destinationBucket: appBucket,
                sources: [s3Deploy.Source.asset(reactAppAsset)],
                distribution,
                distributionPaths: ["/*"],
            });
        
        new CfnOutput(this, 'appUrl', {
            value: distribution.distributionDomainName
        });

        const asset = new ecrAssets.DockerImageAsset(this, 'backed', {
            directory: CdkDeployStack.getProjectPath('ASPNetCoreTraceableAPI'),
            networkMode: ecrAssets.NetworkMode.HOST
        })

        const collectorAsset = new ecrAssets.DockerImageAsset(this, 'collector', {
            directory: 'collector',
            networkMode: ecrAssets.NetworkMode.HOST
        })
        // const vpcToUse = Vpc.fromLookup(this, "eks-vkornev-01-vpc", {
        //     vpcId: "vpc-0fa48d7eb3984779e",
        //     vpcName: "eks-vkornev-01-vpc",
        // });

        // const vpcToUse = Vpc.fromVpcAttributes(this, "eks-vkornev-01-vpc", {
        //     vpcId: "vpc-0fa48d7eb3984779e",
        //     availabilityZones: ["us-east-1a", "us-east-1d", "us-east-1b"],
        //     vpcCidrBlock:"10.33.0.0/16",
        //     privateSubnetIds:["subnet-041d30f5f0a67c14a", "subnet-0cade2d37aeadcb42", "subnet-057ea04561aec4e44"],
        //     privateSubnetNames:["eks-vkornev-01-vpc-private-us-east-1"],
        //     publicSubnetIds:["subnet-010ee7af2442dc68e","subnet-0b392eb00a1d250ad","subnet-0f5111ce704c6b455"],
        //     publicSubnetNames:["eks-vkornev-01-vpc-public-us-east-1"],
        //     publicSubnetRouteTableIds:["rtb-05d90e81d8cbf91b1","rtb-05d90e81d8cbf91b1","rtb-05d90e81d8cbf91b1"],
        //     privateSubnetRouteTableIds:["rtb-012441248173c6aa0","rtb-012441248173c6aa0","rtb-012441248173c6aa0"],
        // });

        const cluster = eks.Cluster.fromClusterAttributes(this, "eks-vkornev-01",
            {
                clusterName: "eks-vkornev-01",
                clusterEndpoint: "https://0EAEF0B44611B029BED057599E2384A0.gr7.us-east-1.eks.amazonaws.com",
                clusterSecurityGroupId: "sg-049d80f5fa5293329",
                kubectlRoleArn: "arn:aws:iam::431817564189:role/eks-vkornev-01-EksAdmins-role",
                vpc: vpc,
                kubectlPrivateSubnetIds: ["subnet-041d30f5f0a67c14a", "subnet-0cade2d37aeadcb42", "subnet-057ea04561aec4e44"],
                kubectlSecurityGroupId: "sg-049d80f5fa5293329",
            });
        
        const app = new cdk8s.App();
        const Cdk8sManifest = new k8sChart({
            image:asset.imageUri,
            collectorImage:collectorAsset.imageUri,
            replicas:1,
            corsUlr:distribution.distributionDomainName,
            nextUrl:entryApi.url
        }, app, AstNaming.Name("chart"));
        const yaml = app.synthYaml();
        new CfnOutput(this, 'yaml', {
            value: yaml
        });
        cluster.addCdk8sChart(AstNaming.Name("chart"), Cdk8sManifest);
    }

    private static getProjectPath(project: string): string {
        return path.resolve('..', project);
    }

    private static getLambdaProjectPath(lambda: string): string {
        return path.resolve(CdkDeployStack.getProjectPath(lambda), "src", lambda);
    }

    private static lambdaPackage(lambda: string): string {
        const output = `out\\${lambda}.zip`;
        dotnet.lambdaPackage(CdkDeployStack.getLambdaProjectPath(lambda), {outputPackage: output});
        return path.resolve(output);
    }

    private static nmpBuild(project: string, resultPath?: string): string {
        const appDir = CdkDeployStack.getProjectPath(project);
        shell.pushd(appDir);

        npm.install();
        npm.run("build");

        shell.popd();
        return path.join(appDir, resultPath || "build");
    }
}
