﻿import {Chart, ChartProps} from "cdk8s";
import {Construct} from "constructs";

import {AstNaming} from "../AstNaming";
import {IntOrString, KubeDeployment, KubeIngress, KubeNamespace, KubeService} from "../imports/k8s";

export class backendProps {
    image: string;
    collectorImage: string;
    replicas: number;
    corsUlr: string;
    nextUrl: string;
}

export class k8sChart extends Chart {
    constructor(backendProps: backendProps, scope: Construct, id: string, props: ChartProps = {}) {
        super(scope, id, props);
        const containerPort = 80;
        const namespace = new KubeNamespace(this, 'backend', {
            metadata: {
                name: AstNaming.applicationName
            }
        });

        new KubeService(this, 'appservice', {
            metadata: {
                namespace: namespace.name,
                name: AstNaming.Name('app')
            },
            spec: {
                ports: [{port: 80, targetPort: IntOrString.fromNumber(containerPort)}],
                selector: {app: AstNaming.Name('app')}
            }
        });


        new KubeService(this, 'collectorservice', {
            metadata: {
                namespace: namespace.name,
                name: AstNaming.Name('collector')
            },
            spec: {
                ports: [{port: 55680, targetPort: IntOrString.fromNumber(55680)}],
                selector: {collector: AstNaming.Name('collector')}
            }
        });

        new KubeDeployment(this, 'deployment', {
            metadata: {
                namespace: namespace.name,
                name: AstNaming.Name('deployment')
            },
            spec: {
                replicas: backendProps.replicas,
                selector: {
                    matchLabels: {app: AstNaming.Name('app'), collector: AstNaming.Name('collector')}
                },
                template: {
                    metadata: {
                        name: AstNaming.Name('deployment'),
                        labels: {app: AstNaming.Name('app'), collector: AstNaming.Name('collector')}
                    },
                    spec: {
                        containers: [
                            {
                                name: 'app',
                                image: backendProps.image,
                                ports: [{containerPort}],
                                env: [
                                    {name: "CORS_URL", value: backendProps.corsUlr},
                                    {name: "LAMBDA_URL", value: backendProps.nextUrl},
                                ]
                            },
                            {
                                name: 'collector',
                                image: backendProps.collectorImage,
                                ports: [{containerPort: 4317},{containerPort: 55680}],
                                //command: ['--config=/etc/config.yaml']
                            }
                        ]
                    }
                }
            }
        });

        new KubeIngress(this, 'ingress', {
            metadata: {
                namespace: namespace.name,
                name: "ingress-router",
                annotations: {
                    "nginx.ingress.kubernetes.io/rewrite-target": "/$2",
                    "ingressclass.kubernetes.io/is-default-class": "true",
                    "kubernetes.io/ingress.class": "nginx",
                    "nginx.ingress.kubernetes.io/enable-cors":"true",
                    "nginx.ingress.kubernetes.io/cors-allow-origin":`https://${backendProps.corsUlr}`
                }
            },
            spec: {
                rules: [
                    {
                        host: AstNaming.Name("backend.aws-team0-dev.orpheusdev.net"),
                        http: {
                            paths: [
                                {
                                    path: "/api(/|$)(.*)",
                                    pathType: "Prefix",
                                    backend: {
                                        service: {
                                            name: AstNaming.Name('app'),
                                            port: {
                                                number: 80
                                            }
                                        }
                                    }
                                },
                                // {
                                //     path: "/tracing(/|$)(.*)",
                                //     pathType: "Prefix",
                                //     backend: {
                                //         service: {
                                //             name: AstNaming.Name('collector'),
                                //             port: {
                                //                 number: 4317
                                //             }
                                //         }
                                //     }
                                // },
                                {
                                    path: "/tracing(/|$)(.*)",
                                    pathType: "Prefix",
                                    backend: {
                                        service: {
                                            name: AstNaming.Name('collector'),
                                            port: {
                                                number: 55680
                                            }
                                        }
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        })
    }
}