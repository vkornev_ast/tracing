﻿export class AstNaming {
    private static _applicationName: string;
    private static _componentName: string;
    private static _envName: string;

    public static set envName(value:string) {
        this._envName = value;
    };
    
    public static set applicationName(value:string) {
        this._applicationName = value;
    };

    public static get applicationName(): string {
        return AstNaming._applicationName + "-" + AstNaming._envName
    };

    public static set componentName(value:string) {
        this._componentName = value;
    };

    public static get componentName(): string {
        return AstNaming._componentName + "-" + AstNaming._envName
    };

    public static Name(name: string): string {
        return AstNaming.applicationName + "-" + name;
    }
}