﻿import {BatchSpanProcessor, ConsoleSpanExporter, SimpleSpanProcessor} from '@opentelemetry/sdk-trace-base';
import {WebTracerProvider} from '@opentelemetry/sdk-trace-web';
import {OTLPTraceExporter } from '@opentelemetry/exporter-otlp-http';
import {Resource} from "@opentelemetry/resources";
import {registerInstrumentations} from "@opentelemetry/instrumentation";
import {FetchInstrumentation} from "@opentelemetry/instrumentation-fetch";
import {AWSXRayPropagator} from "@opentelemetry/propagator-aws-xray";
import {AWSXRayIdGenerator} from "@opentelemetry/id-generator-aws-xray";
import {ZoneContextManager} from "@opentelemetry/context-zone";
import {BaseOpenTelemetryComponent} from "@opentelemetry/plugin-react-load";

const {TraceIdRatioBasedSampler} = require("@opentelemetry/core");

export default (serviceName) => {
    console.log(serviceName);
    //setup attributes to be use along with tracing ids
    // service.name is a reserved OpenTelemetry name to tie all spans from one component
    const resource = new Resource({'service.name': serviceName})
    
    //here we setup:
    // idGenerator to have X-Ray compatible correlation ids
    // resource defined above
    // sampler with corresponding ratio (1 should be used only for debug purposes!)
    const provider = new WebTracerProvider({
        idGenerator: new AWSXRayIdGenerator(),
        resource: resource,
        sampler: new TraceIdRatioBasedSampler(1/2)
    })
    //here we setup:
    //propagator to send x-amzn-trace-id header instead of W3C header which is in use for OpenTelemetry
    provider.register({
        contextManager: new ZoneContextManager(),
        propagator: new AWSXRayPropagator(),
    });
    
    //here is a setup for instrumenting fetch calls. here just default setup 
    registerInstrumentations({
        instrumentations: [new FetchInstrumentation()]
    });

    const collectorUrl = 'https://obs-m-tech-dev-archkorn-backend.aws-team0-dev.orpheusdev.net/tracing/v1/traces';
    console.log(collectorUrl);
    //this is setup for OpenTelemetry Exporter to export telemetry data. api/tracing/ part of url will be overwritten in middleware proxy with sidecar address
    const exporter = new OTLPTraceExporter({
        url: collectorUrl,
        serviceName: serviceName
    });
    provider.addSpanProcessor(new BatchSpanProcessor(exporter));
    
    // to see tracing data in console for debug purposes here is the setup for exporting telemetry data in console
    provider.addSpanProcessor(new SimpleSpanProcessor(new ConsoleSpanExporter()));

    BaseOpenTelemetryComponent.setTracer(serviceName);
}