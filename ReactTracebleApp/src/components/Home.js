import React, {Component} from 'react';
import {withSpan} from "./spanhelper";

export class Home extends Component {
    static displayName = Home.name;

    constructor(props) {
        super(props);
        this.state = {data: "", loading: true};
    }

    async componentDidMount() {
        //create span, which will be reported to telemetry 
        withSpan("ping",(async span => {
            //to use correlation id in logs:
            console.log(`PING @corr-trace-id = ${span.spanContext().traceId}, @corr-span-id=${span.spanContext().spanId}`)
            //this call will create the next span with parent id = span.spanContext().spanId
            //the x-amzn-trace-id will be sent along with request
            const response = await fetch('https://obs-m-tech-dev-archkorn-backend.aws-team0-dev.orpheusdev.net/api/go/ping');
            const content  = await response.text();
            this.setState({data:content, loading: false})
        }))
    }
    
    static renderData(data) {
        return (
            <div>{data}</div>
        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : Home.renderData(this.state.data);
        return (
            <div>
                <h1>Simple test</h1>
                <div>{contents}</div>
            </div>
        );
    }
    
}


