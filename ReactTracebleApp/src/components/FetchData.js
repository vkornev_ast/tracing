import React, {Component} from 'react';
import {BaseOpenTelemetryComponent} from '@opentelemetry/plugin-react-load'
import * as api from '@opentelemetry/api';
import {withSpan} from "./spanhelper";


export class FetchData extends BaseOpenTelemetryComponent {
    testNoVpc = "lambdaNoVpc";
    testVpc = "lambdaVPC";
    testXray = "lambdaXray";
    testXrayVpc = "LambdaXrayVpc";

    static displayName = FetchData.name;
    static zeroPad = (num, places) => String(num).padStart(places, '0')

    constructor(props) {
        super(props);
        this.state = {traceInfo: [], loading: true};

    }

    componentDidMount() {
        this.populateTraceInfo().then();
    }

    static renderTraceInfo(traceInfo) {
        return (
            <ul>
                {traceInfo.map((e, i) => <li key={i}>{e}</li>)}
            </ul>
        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : FetchData.renderTraceInfo(this.state.traceInfo);

        return (

            <div>
                <style>{`.btn {margin: 10}`}</style>
                <p>This component demonstrates sending telemetry during iteracting with backend</p>
                <button className='btn' onClick={this.populateLambdaAPMMultipleTimes}>Run tracing to APM</button>
                <button className='btn' onClick={this.populateLambdaAPMVPCMultipleTimes}>Run tracing to APM via VPC
                </button>
                <button className='btn' onClick={this.populateLambdaXRayMultipleTimes}>Run tracing to X-Ray</button>
                <button className='btn' onClick={this.testTracing}>Run tracing X-Ray vs APM</button>
                {contents}
            </div>
        );
    }

    testTracing = () => {
        this.setState({traceInfo: [0], loading: true});
        const all = [];
        for (let i = 0; i < 30; i++) {
            all.push(this.populateTraceStat(this.testVpc).then());
            all.push(this.populateTraceStat(this.testXray).then());
            all.push(this.populateTraceStat(this.testXrayVpc).then());
        }
        Promise.all(all).then(() => {
            this.setState({traceInfo: ["done"], loading: false});
        });

    }

    incCounter() {
        const counter = this.state.traceInfo[0]++;
        this.setState({traceInfo: [counter], loading: false});
    }

    async populateTraceStat(testMode) {
// Replace the current active span with a new child span
        const context = api.context.active();
        if (context) {
            const span = api.trace.getSpan(context);
            if (span)
                console.log(`GO ${testMode} @corr-trace-id = ${span.spanContext().traceId}, @corr-span-id=${span.spanContext().spanId}, @corr-parent-id=${span.parentSpanId}`)
        }
        const response = await fetch(`https://obs-m-tech-dev-archkorn-backend.aws-team0-dev.orpheusdev.net/api/go/${testMode}`);
        await response.text();

    }

    populateLambdaAPMMultipleTimes = () => {
        this.populateLambdaTestMultipleTimes(this.testNoVpc);
    }

    populateLambdaAPMVPCMultipleTimes = () => {
        this.populateLambdaTestMultipleTimes(this.testVpc);
    }

    populateLambdaXRayMultipleTimes = () => {
        this.populateLambdaTestMultipleTimes(this.testXray);
    }

    populateLambdaTestMultipleTimes(testMode) {
        this.setState({traceInfo: [], loading: true});
        for (let i = 0; i < 5; i++) {
            withSpan(`${testMode} ${FetchData.zeroPad(i, 2)}`, _ => this.populateTraceInfo(testMode).then()).then();
        }
    }

    async populateTraceInfo(testMode) {
// Replace the current active span with a new child span
        const context = api.context.active();
        if (context) {
            const span = api.trace.getSpan(context);
            if (span)
                console.log(`GO ${testMode} @corr-trace-id = ${span.spanContext().traceId}, @corr-span-id=${span.spanContext().spanId}, @corr-parent-id=${span.parentSpanId}`)
        }
        const response = await fetch(`https://obs-m-tech-dev-archkorn-backend.aws-team0-dev.orpheusdev.net/api/go/${testMode}`);
        const data = await response.text();
        const newState = [...this.state.traceInfo, data];
        this.setState({traceInfo: newState, loading: false});
    }
}