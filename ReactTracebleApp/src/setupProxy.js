const createProxyMiddleware = require('http-proxy-middleware');
const {env} = require('process');

const target = env.ASPNETCORE_HTTPS_PORT ? `https://localhost:${env.ASPNETCORE_HTTPS_PORT}` :
    env.ASPNETCORE_URLS ? env.ASPNETCORE_URLS.split(';')[0] : 'https://localhost:7288';

module.exports = function (app) {
   
    const apiPath = '/api';
    const appProxy = createProxyMiddleware(apiPath, {
        target: target,
        secure: false,
        pathRewrite: {[`^${apiPath}`]: ''}
    });
    app.use(appProxy);
    console.log(target);
    //OpenTelemetry collector sidecar url
    //const collectorUrl = 'http://localhost:55680';
    // const tracePath = '/tracing';
    // const traceProxy = createProxyMiddleware(tracePath, {
    //     target: collectorUrl,
    //     secure: false,
    //     pathRewrite: {[`^${tracePath}`]: '/v1/trace'}
    // })
    // app.use(traceProxy);
};
