﻿using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;
using Serilog;
using Serilog.Enrichers.Span;
using Serilog.Formatting.Json;

namespace WebTracableApplication.Controllers
{
    public class HomeController : Controller
    {
        private readonly HttpClient _httpClient = new HttpClient();

        private readonly ILogger _logger = new LoggerConfiguration()
            .Enrich.WithSpan(new SpanOptions{LogEventPropertiesNames = new SpanLogEventPropertiesNames
            {
                ParentId = "@corr-parent-id",
                SpanId = "@corr-span-id ",
                TraceId = "@corr-trace-id"
            }})
            .WriteTo.Console()
            .WriteTo.Console(new JsonFormatter())
            .CreateLogger();

        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Contact()
        {
            //this is an example span to demonstrate
            var activity =
                Activity.Current.Source.StartActivity("Test Span in Contact");
            try
            {
                _logger.Information("test log");
                //thanks to HttpClientInstrumentation the request will be sent with x-amzn-trace-id which carry info about current span
                var response =
                    await _httpClient.GetAsync(
                        "https://4ttr3zdj3m.execute-api.us-east-2.amazonaws.com/prod/api/v1/test");
                var content = await response.Content.ReadAsStringAsync();
                ViewBag.Message = content ?? "no trace id";
                return View();
            }
            finally
            {
                if (activity != null)
                {
                    activity.Stop();
                    _logger.Information("{Activity} ended in {Duration}", activity.DisplayName,
                        activity.Duration.ToString("g"));
                    
                }
            }
        }
    }
}