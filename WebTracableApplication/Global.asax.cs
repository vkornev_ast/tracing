﻿using System;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using OpenTelemetry;
using OpenTelemetry.Contrib.Extensions.AWSXRay.Trace;
using OpenTelemetry.Resources;
using OpenTelemetry.Trace;

namespace WebTracableApplication
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            var serviceName = "WebTracableApplication.Net48";
            Sdk.CreateTracerProviderBuilder()
                //setup attributes to be use along with tracing ids
                // WebTracableApplication.Net48 - is the name for service 
                .SetResourceBuilder(
                                    ResourceBuilder.CreateDefault()
                                        .AddService(serviceName: serviceName).AddTelemetrySdk())
                //.AddSource(serviceName)
                //X-Ray compatible correlation ids
                .AddXRayTraceId()
                // add instrumentations for http requests. If you need to instrument other clients - just try to search OpenTelemetry.Instrumentation packages.
                .AddHttpClientInstrumentation()
                // add instrumentations for  controllers
                .AddAspNetInstrumentation()
                // setup of side-car which will get data in OTLP format
                .AddOtlpExporter(options => { options.Endpoint = new Uri("http://localhost:4317"); })
                .Build();
            Sdk.SetDefaultTextMapPropagator(new AWSXRayPropagator());
           
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}